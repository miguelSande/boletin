-module(sorting).

-export([quicksort/1,mergesort/1]).

quicksort([Piv|T]) ->
  quicksort([X || X <- T, X < Piv])++
  [Piv]++
  quicksort([X || X <- T, X >= Piv]);
quicksort([]) -> [].



mergesort([]) -> [];
mergesort(L) when length(L) > 1->
  {L1,L2} = lists:split(round(length(L)/2),L),
  merge(mergesort(L1),mergesort(L2));
mergesort(L) -> L.

merge(X,Y) -> merge(X,Y,[]).

merge([],[],Acc) -> Acc;
merge([],S2,Acc) -> Acc ++ quicksort(S2);
merge(S1,[],Acc) -> Acc ++ quicksort(S1);
merge([H1|T1],[H2|T2],Acc) when H1 >= H2 ->  merge([H1|T1],T2,Acc++[H2]);
merge([H1|T1],[H2|T2],Acc) ->  merge(T1,[H2|T2],Acc++[H1]).





