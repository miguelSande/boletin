
-module(crossring).

-export([start/3,create/3,loop/3,loop/1,loop/0]).

proc(Sig) ->receive
		{_,0,_}->Sig ! stop,ok;		
		{_,MsgNum,Mensaje}->Sig ! {self(),MsgNum-1,Mensaje},proc(Sig);
		stop->Sig ! stop,ok;					
		_->proc(Sig)
	end.

loop() -> receive
  {From,Mnum,Msg} -> whereis(derecha) ! {self(),Mnum,Msg},loop(From);

  stop -> ok;

  _ -> loop()
 end.

loop(Der) -> receive
  {Der,Mnum,Msg} -> whereis(derecha) ! {self(),Mnum,Msg},loop(Der);

  {From,Mnum,Msg} -> whereis(izquierda) ! {self(),Mnum,Msg}, loop(Der,From,1);

  stop -> ok;

  _ -> loop(Der)
 end.

loop(Der,Izq,Count) -> receive
  {Der,0,_} -> whereis(derecha) ! stop, loop(Der,Izq,Count);

  {Der,Mnum,Msg} -> whereis(derecha) ! {self(),Mnum,Msg}, loop(Der,Izq,Count);

  {Izq,0,_} -> whereis(izquierda) ! stop, loop(Der,Izq,Count);

  {Izq,Mnum,Msg} -> whereis(izquierda) ! {self(),Mnum,Msg}, loop(Der,Izq,Count);

  stop when Count =:= 0-> ok;

  stop -> loop(Der,Izq,Count-1);

 _ -> loop(Der,Izq,Count) 
end.

create(1,MsgNum,Message)->whereis(master) ! {MsgNum-1,Message},proc(whereis(master));
create(ProcNum,MsgNum,Message)->proc(spawn(?MODULE,create,[ProcNum-1,MsgNum,Message])).

start(ProcNum, MsgNum, Message) when ProcNum>0 ,MsgNum>0->
   register(master,spawn(?MODULE,loop,[])),
   register(derecha,spawn(?MODULE,create,[(ProcNum-1) div 2,MsgNum,Message])),
   register(izquierda,spawn(?MODULE,create,[ProcNum-((ProcNum-1) div 2)-1,MsgNum,Message])),ok;
start(_,_,_)->"Numero de procesos y numero de mensajes debe ser mayor que 0".

















