
-module(crossring).

-export([start/3,create/4,proc/1,proc/2]).

search(_,_,[]) -> [];
search(_,N,L) when (length(L) =:= 2) and (N > length(L))-> {_,Pid} = lists:nth(1,L), Pid; 
search(_,N,L) when N > length(L) -> {_,Pid} = lists:nth((length(L) div 2) +1,L), Pid;
search(F,N,L) when (F =:= length(L)) and (N >= length(L)) -> {_,Pid} = lists:nth(N,L), Pid;
search(F,_,L) when F =:= length(L) -> {_,Pid} = lists:nth(1,L), Pid;
search(_,N,L) -> {_,Pid} = lists:nth(N,L), Pid.

distribuir([]) -> ok;
distribuir(L) -> distribuir(L,L).

distribuir(_,[]) -> ok;
distribuir(L,[{_,Pid}|T]) -> Pid ! L, distribuir(L,T).


proc(Num,List) ->receive
                {From,_,0} when length(List) < 3 -> (search(From,Num+1,List)) ! {Num,stop},ok;
		{From,_,0} when Num =:= ((length(List) div 2) +1)-> (search(1,Num+1,List)) ! {Num,stop},(search(From,1,List)) ! {Num,stop},ok;	
                {From,_,0}-> (search(From,Num+1,List)) ! {Num,stop},ok;	
                {From,Mensaje,MsgNum} when length(List) < 3 -> (search(1,Num+1,List)) ! {Num,Mensaje,MsgNum-1},proc(Num,List);
		{From,Mensaje,MsgNum}-> (search(From,Num+1,List)) ! {Num,Mensaje,MsgNum-1},proc(Num,List);
                {From,stop} when length(List) < 3 -> (search(1,Num+1,List)) ! {Num,stop},ok;
		{From,stop} when Num =:= ((length(List) div 2) +1)-> (search(From,1,List)) ! {Num,stop},(search(1,Num+1,List)) ! {Num,stop},ok;
                {From,stop}-> (search(From,Num+1,List)) ! {Num,stop},ok;
                				
		_->proc(Num,List)
	end.

proc(Num) -> receive
 L when is_list(L) -> proc(Num,L);

 _ -> proc(Num)
end.

create(0,MsgNum,Message,List) -> distribuir(lists:reverse(List)),timer:sleep(100), (search(1,1,lists:reverse(List))) ! {1,Message,MsgNum-1};
create(ProcNum,MsgNum,Message,List)->Pid = spawn(?MODULE,proc,[ProcNum]),create(ProcNum -1, MsgNum,Message,List++[{ProcNum,Pid}]).

start(ProcNum, MsgNum, Message) when ProcNum>0 ,MsgNum>0-> create(ProcNum,MsgNum,Message,[]),ok;
start(_,_,_)->"Numero de procesos y numero de mensajes debe ser mayor que 0".

















