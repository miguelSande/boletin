
-module(ring).

-export([start/3,create/3]).

proc(Sig) ->receive
		{0,_Mensaje}->Sig ! stop,ok;		
		{MsgNum,Mensaje}->Sig ! {MsgNum-1,Mensaje},proc(Sig);
		stop->Sig ! stop,ok;					
		_->proc(Sig)
	end.

create(0,MsgNum,Message)->whereis(proc0) ! {MsgNum-1,Message},proc(whereis(proc0));
create(ProcNum,MsgNum,Message)->proc(spawn(?MODULE,create,[ProcNum-1,MsgNum,Message])).

start(ProcNum, MsgNum, Message) when ProcNum>0 ,MsgNum>0->register(proc0,spawn(?MODULE,create,[ProcNum-1,MsgNum,Message])),ok;
start(_,_,_)->"Numero de procesos y numero de mensajes debe ser mayor que 0".

















