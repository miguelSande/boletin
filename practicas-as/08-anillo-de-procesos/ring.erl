-module(ring).

-export([start/3,strat/3]).

loop(Next) ->
 receive
  stop -> Next ! stop,ok;

  {0,_} -> Next ! stop,ok;

  {MNum,Msg} -> Next ! {MNum-1,Msg}, loop(Next);

  _ -> loop(Next)
 end.

strat(PNum,Mnum,Msg) when PNum =:= 0 ->
 whereis(first) ! {Mnum-1,Msg},
 loop(whereis(first));
strat(PNum,Mnum,Msg) ->
 Pid = spawn(ring,strat,[PNum-1,Mnum,Msg]),
 loop(Pid).

start(ProcNum,MsgNum,Message) -> Pid = spawn(ring,strat,[ProcNum-1,MsgNum,Message]), register(first,Pid).




