-module(effects).

-export([print/1,even_print/1]).

print([H]) -> io:format("~p~n",[H]);
print([H|T]) ->
  io:format("~p~n",[H]),
  print(T);
print(N) -> print(lists:seq(1,N)).

even_print([]) -> io:format("");
even_print([H|T]) when H rem 2 /= 0 -> even_print(T);
even_print([H|T]) ->
  io:format("~p~n",[H]),
  even_print(T);
even_print(N) -> even_print(lists:seq(1,N)).
