-module(create).

-export([create/1,reverse_create/1]).

create(N) -> rec_create(1,N,[]).

rec_create(C,N,L) when C =:= N -> L++[N];
rec_create(C,N,L) -> rec_create(C+1,N,L++[C]).

reverse_create(N) -> rec_reverse(N,[]).

rec_reverse(0,L) -> L;
rec_reverse(N,L) -> rec_reverse(N-1,L++[N]).
