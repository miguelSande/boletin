-module(manipulating).

-export([filter/2,reverse/1,concatenate/1,flatten/1]).


filter([],_) -> [];
filter([H|T],N) when H > N -> filter(T,N);
filter([H|T],N) -> [H | filter(T,N)].


reverse(List) -> reverse(List,[]).

reverse([H|T],Acc) -> reverse(T,[H|Acc]);
reverse([],Acc) -> Acc.


concatenate(L) -> concatenate([],L,[]).

concatenate([],[H|T],Acc) -> concatenate(H,T,Acc);
concatenate([],[],Acc) -> reverse(Acc);
concatenate([H|T],Rest,Acc) -> concatenate(T,Rest,[H|Acc]).


flatten(List) -> reverse(flatten(List,[])).

flatten([],Acc) -> Acc;
flatten([H|T],Acc) when is_list(H) -> flatten(T, flatten(H,Acc));
flatten([H|T],Acc) -> flatten(T,[H|Acc]).
