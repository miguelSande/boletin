-module(echo).
-export([start/0,loop/0,stop/0,print/1]).
 
start() -> 
Pid = spawn(echo,loop,[]),
register(echo,Pid),ok.

stop() -> echo ! stop,ok.

print(X) -> echo ! {self(),X},ok.

loop() ->
  receive
   stop -> true;

   {From,X} ->
    From ! X,
    io:format("~p~n",[X]),
    loop()
  end.
